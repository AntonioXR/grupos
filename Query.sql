USE gruposPrueba;
CREATE TABLE seccion(
	idSeccion INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(40) NOT NULL
);

CREATE TABLE grupo(
	idGrupo INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(20) NOT NULL,
	idSeccion INT NOT NULL,
	FOREIGN KEY (idSeccion) REFERENCES seccion(idSeccion)
);
CREATE TABLE curso(
	idCurso INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(30) NOT NULL,
	idGrupo INT NOT NULL,
	FOREIGN KEY (idGrupo) REFERENCES grupo(idGrupo)
);


DELIMITER $$

CREATE PROCEDURE sp_agregarGrupo(
		IN gp_nombre VARCHAR(20),
		IN gp_idSeccion INT )
BEGIN 
		DECLARE va_count INT;
		DECLARE r_esultado INT;
		SET va_count = (SELECT COUNT(idGrupo) FROM grupo WHERE nombre = gp_nombre);
		IF va_count = 0 THEN 
			INSERT INTO grupo (nombre, idSeccion) VALUES(gp_nombre, gp_idSeccion);
			SELECT gr.* , se.nombre AS nombreSeccion FROM grupo gr INNER JOIN seccion se ON se.idSeccion = gr.idSeccion WHERE gr.nombre = gp_nombre;
		ELSE
			SET r_esultado = (1) AS error;
			SELECT r_esultado;
		END IF;		
END $$	

CREATE PROCEDURE sp_agregarCurso(
		IN cr_nombre VARCHAR(20),
		IN cr_idGrupo INT )
BEGIN 
		DECLARE va_count INT;
		DECLARE r_esultado INT;
		SET va_count = (SELECT COUNT(idGrupo) FROM curso WHERE nombre = cr_nombre);
		IF va_count = 0 THEN 
			INSERT INTO curso (nombre, idGrupo) VALUES(cr_nombre, cr_idGrupo);
			SELECT cr.* , gr.nombre AS nombreGrupo FROM curso cr INNER JOIN grupo gr ON cr.idGrupo = gr.idGrupo WHERE cr.nombre = cr_nombre;
		ELSE
			SET r_esultado = (1);
			SELECT r_esultado AS error;
		END IF;		
END $$

DELIMITER ;

CREATE VIEW vw_showCurso
AS
	SELECT cr.* , gr.nombre AS nombreGrupo FROM curso cr INNER JOIN grupo gr ON cr.idGrupo = gr.idGrupo;

