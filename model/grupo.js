var database = require('./database');
var grupo = {};

grupo.selectAll = function(callback) {
  if(database) {
    database.query("SELECT gr.* , se.nombre AS nombreSeccion FROM grupo gr INNER JOIN seccion se ON se.idSeccion = gr.idSeccion ORDER BY se.idSeccion ;",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

grupo.select = function(idGrupo, callback) {
  if(database) {
    var sql = "SELECT gr.* , se.nombre AS nombreSeccion FROM grupo gr INNER JOIN seccion se ON se.idSeccion = gr.idSeccion WHERE idGrupo = ? ORDER BY se.idSeccion ";
    database.query(sql, idGrupo,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

grupo.insert = function(data, callback) {
  if(database) {
    database.query("CALL  sp_agregarGrupo(?,?);", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

grupo.update = function(data, callback) {
  if(database) {
    var sql = "UPDATE grupo SET nombre = ?, idSeccion = ? WHERE idGrupo = ?";
    database.query(sql, data, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

grupo.delete = function(idGrupo, callback) {
  if(database) {
    var sql = "DELETE FROM grupo WHERE idGrupo = ?";
    database.query(sql, idGrupo,
    function(error, idGrupo) {
      if(error) {
        //throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = grupo;
