var database = require('./database');
var curso = {};

curso.selectAll = function(callback) {
  if(database) {
    database.query("SELECT cr.* , gr.nombre AS nombreGrupo FROM curso cr INNER JOIN grupo gr ON cr.idGrupo = gr.idGrupo ORDER BY gr.idGrupo;",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

curso.select = function(idGrupo, callback) {
  if(database) {
    var sql = "SELECT cr.* , gr.nombre AS nombreGrupo FROM curso cr INNER JOIN grupo gr ON cr.idGrupo = gr.idGrupo WHERE idCurso = ? ORDER BY gr.idGrupo;";
    database.query(sql, idGrupo,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

curso.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_agregarCurso(?,?);", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

curso.update = function(data, callback) {
  if(database) {
    var sql = "UPDATE curso SET nombre = ?, idGrupo = ? WHERE idCurso = ?";
    database.query(sql, data, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

curso.delete = function(idGrupo, callback) {
  if(database) {
    var sql = "DELETE FROM grupo WHERE idCurso = ?";
    database.query(sql, idCategoria,
    function(error, idGrupo) {
      if(error) {
        //throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = curso;
