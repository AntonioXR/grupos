var database = require('./database');
var seccion = {};

seccion.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM seccion",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

seccion.select = function(idCategoria, callback) {
  if(database) {
    var sql = "SELECT * FROM seccion WHERE idSeccion = ?";
    database.query(sql, idCategoria,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

seccion.insert = function(data, callback) {
  if(database) {
    database.query("INSERT INTO seccion(nombre) VALUES(?) ", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

seccion.update = function(data, callback) {
  if(database) {
    var sql = "UPDATE seccion SET nombre = ? WHERE idSeccion = ?";
    database.query(sql, data, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

seccion.delete = function(idCategoria, callback) {
  if(database) {
    var sql = "DELETE FROM seccion WHERE idSeccion = ?";
    database.query(sql, idCategoria,
    function(error, resultado) {
      if(error) {
        //throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = seccion;
