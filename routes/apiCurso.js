var express = require('express');
var curso = require('../model/curso');
var router = express.Router();

router.get('/api/curso/', function(req, res) {
  curso.selectAll(function(error, resultados){
    if(typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay categorias"});
    }
  });
});

router.get('/api/curso/:idCurso',
  function(req, res) {
    var idCurso = req.params.idCurso;
    curso.select(idCurso,
      function(error, resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No hay categorias"});
      }
  });
});

router.post('/api/curso', function(req, res) {
  var data = [req.body.nombre, req.body.idGrupo]
  curso.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
       res.json({"Mensaje":true});
    } else {
      res.json({"Mensaje": false});
    }
  });
});

router.put('/api/curso/:idCurso', function(req, res) {
  var idCurso = req.params.idCurso;
  var data = [
    req.body.nombre,
    req.body.idGrupo,
    req.body.idCurso
  ]                                                                                             

  if(idCurso == req.body.idCurso) {
    curso.update(data, function(err, resultado) {
      if(typeof resultado !== undefined) {
        res.json(resultado);
      } else {
        res.json({"Mensaje": "No se modifico la categoria"});
      }
    });
  } else {
    res.json({"Mensaje": "No concuerdan los datos"});
  }
});

router.delete('/api/curso/',
  function(req, res) {
    var idCurso = req.body.idCurso;
    curso.delete(idCurso,
      function(error, resultado){
      if(resultado && resultado.Mensaje == "Eliminado") {
        res.json({"Mensaje": true});
      } else {
        res.json({"Mensaje": false});
      }
  });
});


module.exports = router;
