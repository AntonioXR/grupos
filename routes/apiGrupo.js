var express = require('express');
var grupo = require('../model/grupo');
var router = express.Router();

router.get('/api/grupo/', function(req, res) {
  grupo.selectAll(function(error, resultados){
    if(typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay categorias"});
    }
  });
});

router.get('/api/grupo/:idgrupo',
  function(req, res) {
    var idgrupo = req.params.idgrupo;
    grupo.select(idgrupo,
      function(error, resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No hay categorias"});
      }
  });
});

router.post('/api/grupo', function(req, res) {
  var data = [req.body.nombre, req.body.idSeccion]
  grupo.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
       res.json({"Mensaje":true});
    } else {
      res.json({"Mensaje": false});
    }
  });
});

router.put('/api/grupo/:idgrupo', function(req, res) {
  var idgrupo = req.params.idgrupo;
  var data = [
    req.body.nombre,
    req.body.idSeccion,
    req.body.idGrupo
  ]                                                                                             

  if(idgrupo == req.body.idGrupo) {
    grupo.update(data, function(err, resultado) {
      if(typeof resultado !== undefined) {
        res.json(resultado);
      } else {
        res.json({"Mensaje": "No se modifico la categoria"});
      }
    });
  } else {
    res.json({"Mensaje": "No concuerdan los datos"});
  }
});

router.delete('/api/grupo/',
  function(req, res) {
    var idgrupo = req.body.idGrupo;
    grupo.delete(idgrupo,
      function(error, resultado){
      if(resultado && resultado.Mensaje == "Eliminado") {
        res.json({"Mensaje": true});
      } else {
        res.json({"Mensaje": false});
      }
  });
});


module.exports = router;
