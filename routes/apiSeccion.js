var express = require('express');
var seccion = require('../model/seccion');
var router = express.Router();

router.get('/api/seccion/', function(req, res) {
  seccion.selectAll(function(error, resultados){
    if(typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay categorias"});
    }
  });
});

router.get('/api/seccion/:idSeccion',
  function(req, res) {
    var idSeccion = req.params.idSeccion;
    seccion.select(idSeccion,
      function(error, resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No hay categorias"});
      }
  });
});

router.post('/api/seccion', function(req, res) {
  var data = req.body.nombre
  seccion.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
       res.json({"Mensaje":true});
    } else {
      res.json({"Mensaje": false});
    }
  });
});

router.put('/api/seccion/:idSeccion', function(req, res) {
  var idSeccion = req.params.idSeccion;
  var data = [
    req.body.nombre,
    req.body.idSeccion
  ]                                                                                             

  if(idSeccion == req.body.idSeccion) {
    seccion.update(data, function(err, resultado) {
      if(typeof resultado !== undefined) {
        res.json(resultado);
      } else {
        res.json({"Mensaje": "No se modifico la categoria"});
      }
    });
  } else {
    res.json({"Mensaje": "No concuerdan los datos"});
  }
});

router.delete('/api/seccion/',
  function(req, res) {
    var idSeccion = req.body.idSeccion;
    seccion.delete(idSeccion,
      function(error, resultado){
      if(resultado && resultado.Mensaje == "Eliminado") {
        res.json({"Mensaje": true});
      } else {
        res.json({"Mensaje": false});
      }
  });
});


module.exports = router;
